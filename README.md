# README #

This is test service for payout through `https://paymega.io/`

### How to start the service ###
1. Log in into your account `https://my.paymega.io/login`
2. Install needed libraries(need if you run separate file)

```
  gem install httparty
```
3. 
```
  paymega_service = PaymegaService.new(account_id, api_key)
  paymega_service.pay(data)
  paymega_service.payout(data)
  paymega_service.parse_callback(data) => # "process_pending"
```
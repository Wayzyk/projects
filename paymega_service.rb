# frozen_string_literal: true

require 'rubygems'
require 'httparty'
require 'ostruct'

class PaymegaService
  include HTTParty

  base_uri 'https://api.paymega.io'
  PAY_URL = '/public-api/payment-invoices'
  PAYOUT_URL = '/public-api/payout-invoices'

  def initialize(**args)
    @account_id = args[:account_id]
    @api_key = args[:api_key]
  end

  def parse_callback(data)
    response = parse_response(data, OpenStruct)
    response.data.attributes.status
  end

  def pay(data)
    response = send_request(PAY_URL, data)
    parse_response(response.body)
  end

  def payout(data)
    response = send_request(PAYOUT_URL, data)
    parsed_response = parse_response(response.body)
    process_payout(parsed_response[:data][:id]) if response.response.code == '201'
    parsed_response
  end

  private

  def process_payout(payout_id)
    url = "#{PAYOUT_URL}/#{payout_id}/process"
    send_request(url)
  end

  def send_request(uri, options = nil)
    self.class.post(uri, headers: { "Authorization": "Basic #{encode_token}" }, body: options)
  end

  def encode_token
    encode64("#{@account_id}:#{@api_key}")
  end

  def parse_response(body, klass = nil)
    JSON.parse(body, object_class: klass, symbolize_names: true)
  end
end